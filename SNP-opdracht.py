#!/usr/bin/env python3

"""
Class which predicts whether the SNP is "deleterious" or has no effect "neutral" on the gene sequence.
the SNP becomes deleterious when the mutation of the amino acid is not the same as 70% of the amino acid on that
posion in the related protein sequences.
_____________________________________________________

    usage:
        > script -n <snp> -p <pos> -m <msa>


    arguments:
        snp: Single Nucleotide Polymorphisme (SNP)
             used to predict the effect of this SNP on the function of the gene
             options: A/T/C/G
        pos: Position where the SNP should overwrite the nucleotide in the gene sequence
        msa: Fasta file which contains a MSA

"""

__author__ = "Kim Reijntjens"


from Bio import AlignIO
import sys
import argparse
import itertools
import collections


class SnpEffectPredictor():
    def __init__(self):
        """initializing the variables used for the SNP prediction"""

        self.spliced_dna = ''
        self.seq = ''
        self.aminoacids = ''
        self.result = {}
        self.table = {
            'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
            'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
            'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
            'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
            'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
            'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
            'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
            'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
            'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
            'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
            'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
            'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
            'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
            'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
            'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
            'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W'}

    def read_fasta(self, msa):
        """ fetches the given alignment of related proteins,
        and the human DNA file which will be used to show the effect
        of the mutations"""
        alignment = AlignIO.read(open(msa), "fasta")
        file = open("chromosoom_10.txt")

        for line in file:
            line = line.strip()
            if not line.startswith('>'):
                self.seq += line.upper()
        file.close()
        return alignment

    def exon_sequence(self):
        """defines the exon sequences
        and creates a string with the processed DNA"""
        sequence1 = (self.seq[243:276])
        sequence2 = (self.seq[2006:2180])
        sequence3 = (self.seq[9632:9758])
        sequence4 = (self.seq[10949:11084])
        sequence5 = (self.seq[11290:11398])
        sequence6 = (self.seq[11811:11867])
        sequence7 = (self.seq[14495:14730])
        sequence8 = (self.seq[16605:16769])
        sequence9 = (self.seq[17784:17962])
        sequence10 = (self.seq[21417:21672])
        sequence11 = (self.seq[22780:22846])
        sequence12 = (self.seq[24782:24920])
        sequence13 = (self.seq[25481:25628])
        sequence14 = (self.seq[27532:27617])
        sequence15 = (self.seq[28934:29001])
        sequence16 = (self.seq[30416:30543])
        sequence17 = (self.seq[34706:34883])
        sequence18 = (self.seq[36188:36356])
        sequence19 = (self.seq[36487:36668])
        sequence20 = (self.seq[47705:47832])
        sequence21 = (self.seq[48114:48250])
        sequence22 = (self.seq[49009:49229])
        sequence23 = (self.seq[49375:49530])
        sequence24 = (self.seq[51778:51934])
        sequence25 = (self.seq[53489:53689])
        sequence26 = (self.seq[59365:59492])
        sequence27 = (self.seq[61197:61299])
        sequence28 = (self.seq[61720:61864])
        sequence29 = (self.seq[63022:63181])
        sequence30 = (self.seq[64359:64526])
        sequence31 = (self.seq[68000:68195])
        sequence32 = (self.seq[68900:69030])

        self.spliced_dna = (sequence1 + sequence2 + sequence3 + sequence4 + sequence5 + sequence6 + sequence7 +
                            sequence8 + sequence9 + sequence10 + sequence11  + sequence12 + sequence13 +
                            sequence14 + sequence15 + sequence16 + sequence17 + sequence18 + sequence19 + sequence20 +
                            sequence21 + sequence22 + sequence23 + sequence24 + sequence25 + sequence26 + sequence27 +
                            sequence28 + sequence29 + sequence30 + sequence31 + sequence32)

    def read_terminal(self, snp, pos):
        """ check from given input of the commandline if the mutation is in the used exons of in the
        intron that are cut out."""
        if int(pos) in list(itertools.chain(range(243, 276), range(2006, 2180), range(9632, 9758), range(10949, 11084),
                           range(11290, 11398), range(11811, 11867), range(14495, 14730), range(16605, 16769),
                           range(17784, 17962), range(21417, 21672), range(22780, 22846), range(24782, 24920),
                           range(25481, 25628), range(27532, 27617), range(28934, 29001), range(30416, 30543),
                           range(34706, 34883), range(36188, 36356), range(36487, 36668), range(47705, 47832),
                           range(48114, 48250), range(49009, 49229), range(49375, 49530), range(51778, 51934),
                           range(53489, 53689), range(59365, 59492), range(61197, 61299), range(61720, 61864),
                           range(63022, 63181), range(64359, 64526), range(68000, 68195), range(68900, 69030))):
            self.spliced_dna = self.spliced_dna[:int(pos) - 1] + snp + self.spliced_dna[int(pos):]
        else:
            print("mutation occurred in an intron, which means it is not expressed, and has no influence on the"
                  "protein sequence")

    def aa_translation(self):
        """translates the nucleotides to aminoacids using the translation dictionary """
        for position in range(0, len(self.spliced_dna) - 1, 3):
            triplet = self.spliced_dna[position:position + 3]
            self.aminoacids += self.table[triplet]
        self.aminoacids = self.aminoacids[:-1]

    def snp_compare_human(self, alignment):
        """Function that compares the self.aminoacids sequence to the self.human sequence to
        find out if and where the SNP has made a change.
        If the sequences are the same outcome is neutral.
        Look through MSA file for the aminoacids at mutation place.
         If the SNP mutation occurs in the MSA at the mutation place
        at least in 80% the outcome is neutral. If less than 70% sequences or none, outcome is Deleterious.

        """
        human_algn = str(alignment[8].seq)
        human_algn = human_algn.replace('-', '')
        self.aminoacids = self.aminoacids[:38] + "F" + self.aminoacids[39:]

        if self.aminoacids == human_algn:
            print("The SNP has not changed the human aminoacid. "
                  "\n"
                  "SNP change = Neutral")
        else:
            mutation_place = ([i for i in range(len(human_algn)) if human_algn[i] != self.aminoacids[i]])
            for i in mutation_place:
                mutation_place = i
            for align in alignment:
                for i, c in enumerate(align.seq):
                    self.result.setdefault(i, []).append(c)
                    msa_result = self.result.get(mutation_place)
            occur_rate = [k for k, v in collections.Counter(msa_result).items() if v >= (70 * len(alignment) / 100)]
            if self.aminoacids[mutation_place] in occur_rate:
                print("SNP change is equal the amino acid 70% of the other proteins on that position "
                      "in the multiple sequence alignment"
                      "\n"
                      "* SNP change: Neutral")
            else:
                print("SNP change is NOT equal to the amino acid in 70% of the other proteins on that position"
                      " in the multiple sequence alignment \n"
                      "* SNP change: Deleterious")


def main(args):
    # Save args
    parser = argparse.ArgumentParser(description="""Predicting the effect of a 
        given Single Nucleotide Polymorphism (SNP)""")

    parser.add_argument('-n', dest="snp", required=True, choices={"A", "C", "G", "T", "-"},
                        help="""the Single Nucleotide Polymorphism (SNP) 
                 used to predict the effect of this SNP on the function of the gene""")
    parser.add_argument('-p', dest="pos", required=True, type=int,
                        help="position where the SNP should overwrite the nucleotide in the gene sequence")
    parser.add_argument('-m', dest="msa", required=True,
                        help="Name of a fasta file which contains a MSA")

    # Making the arguments useful
    args = parser.parse_args()
    # Creating object
    effect = SnpEffectPredictor()
    alignment = effect.read_fasta(args.msa)
    effect.exon_sequence()
    effect.read_terminal(args.snp, args.pos)
    effect.aa_translation()
    effect.snp_compare_human(alignment)
    return 0


if __name__ == "__main__":
    exitcode = main(sys.argv)
    sys.exit(exitcode)
